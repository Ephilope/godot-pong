extends Node2D

var speed = 600  # Vitesse de la palette
var _ball_dir
var screen_size
var ball : Node
var _up
var _down
func _ready():
	screen_size = get_viewport_rect().size
	ball = get_node("/root/main/Ball")

func _process(delta):
	if(Global.get_player2orIA()=="ia"):
		if ball.position.x > position.x - 100:
			# Déplacer la palette vers la position y de la balle
			var direction = sign(ball.position.y - position.y)
			position.y += direction * speed * delta
			
			# Empêcher la palette de sortir de l'écran
			position.y = clamp(position.y, 0, get_viewport_rect().size.y)
	else :
		var new_position = position.y  # Nouvelle position calculée de la raquette
		
		# Déplacer la raquette vers le haut
		if Input.is_action_pressed("move_up_j2"):
			new_position -= speed * delta
		
		# Déplacer la raquette vers le bas
		if Input.is_action_pressed("move_down_j2"):
			new_position += speed * delta
		
		# Vérifier si la nouvelle position est dans les limites de l'écran
		position.y = clamp(new_position, 0, screen_size.y)

