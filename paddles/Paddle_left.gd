extends Node2D

var speed = 300  # Vitesse de déplacement de la raquette
var screen_size # Size of the game window.
var ball=load("res://Ball.gd")


func _ready():
	screen_size = get_viewport_rect().size
func _process(delta):
	var new_position = position.y  # Nouvelle position calculée de la raquette
	
	# Déplacer la raquette vers le haut
	if Input.is_action_pressed("move_up"):
		new_position -= speed * delta
	
	# Déplacer la raquette vers le bas
	if Input.is_action_pressed("move_down"):
		new_position += speed * delta
	
	# Vérifier si la nouvelle position est dans les limites de l'écran
	position.y = clamp(new_position, 0, screen_size.y)




