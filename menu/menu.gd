extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_ia_pressed():
	Global.set_player2orIA("ia")
	get_tree().change_scene_to_file("res://main.tscn")
	



func _on_button_2p_pressed():
	Global.set_player2orIA("p2")
	get_tree().change_scene_to_file("res://main.tscn")


func _on_button_quit_pressed():
	get_tree().quit()
