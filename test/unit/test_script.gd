extends "res://addons/gut/test.gd"


func test_void():
	assert_eq("","","Test vide")
	
#                                                                           Test pour la fonction _reset_ball()
# Le nœud de la balle à tester
var ball = preload("res://Ball.gd").instance()

func setup():
	# Ajouter le nœud ball à la scène de test
	add_child(ball)

func test_reset_ball():
	# Appeler la fonction _reset_ball()
	ball._reset_ball()

	# Vérifier si la balle a été réinitialisée au bon endroit
	assert_eq(ball.global_position, Vector2(ball.screen_width / 4, ball.screen_height / 4))

	# Vérifier la vélocité de la balle
	assert_eq(ball.linear_velocity, Vector2())
	
func test_ball_movement():
	# Configurer une position et une vélocité initiale pour la balle
	ball.global_position = Vector2(100, 100)
	ball.linear_velocity = Vector2(100, -100) # la balle se déplace vers le haut

	# Exécuter la fonction _process() de la balle avec un delta de 1 seconde
	ball._process(1)

	# Vérifier si la balle a rebondi sur le bord supérieur
	assert_gt(ball.linear_velocity.y, 0) # la vélocité en y devrait être positive après le rebond

	
	
