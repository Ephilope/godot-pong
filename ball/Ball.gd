extends Node2D


const DEFAULT_SPEED = 300

var _speed = DEFAULT_SPEED
var direction = Vector2.LEFT

var _initial_pos = position
var screen_size
func _process(delta):
	_speed += delta * 2
	position += _speed * delta * direction
func _ready():
	screen_size = get_viewport_rect().size
func change_direction(d):
	direction=Vector2(d, randf() * 2 - 1).normalized()

func reset():
	direction = Vector2.LEFT
	position = _initial_pos
	_speed = DEFAULT_SPEED


func _on_paddle_left_area_area_entered(area):
	if area.name=="Ball_area":
		print(area.name)
		change_direction(1)


func _on_paddle_rigth_area_area_entered(area):
	if area.name=="Ball_area":
		print(area.name)
		change_direction(-1)

func _on_area_2d_area_entered(area):
	print(position.y)
	if area.name=="Ball_area":
		if position.y > screen_size.y/2:
			direction+=Vector2(0,-1).normalized()
		else:
			direction+=Vector2(0,1).normalized()
		


func _on_area_2d_area_exited(area):
	if area.name=="Ball_area":
		reset()
